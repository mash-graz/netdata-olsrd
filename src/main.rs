use anyhow::{bail, Context, Result};
// use eyre::{Result, WrapErr, ContextCompat};
use env_logger::Builder;
use gumdrop::Options;
use log::{debug, error, info, LevelFilter};
use serde::Deserialize;
use std::{io::Read, io::Write, net::SocketAddr, thread, time::Duration};

use netdata_plugin::collector::{Collector, CollectorError};
use netdata_plugin::{Chart, Dimension};

#[cfg(target_arch = "wasm32")]
use lunatic::net::{resolve_timeout, TcpStream, ToSocketAddrs};
#[cfg(not(target_arch = "wasm32"))]
use std::net::{TcpStream, ToSocketAddrs};

#[derive(Deserialize, Debug)]
struct Response {
    neighbors: Option<Vec<Neighbour>>,
    links: Option<Vec<Link>>,
}

#[derive(Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
struct Neighbour {
    #[serde(alias = "ipv4Address")]
    ip_address: String,
    two_hop_neighbor_count: i32,
}

#[allow(non_snake_case)]
#[derive(Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
struct Link {
    local_IP: String,
    remote_IP: String,
    link_quality: f32,
    neighbor_link_quality: f32,
    link_cost: f32,
}

/// A very simple HTTP client and JSON reader
///
/// The network communication is realized without fancy HTTP libraries, because the
/// olsr daemon doesn't behave as a standard compliant web server (i.e. it doesn't send
/// headers in older releases and unfortunately doesn't support 'Keep-Alive' instructions).
fn simple_json_fetch(socket_addr: SocketAddr, sections: &str, timeout: u64) -> Result<Response> {
    let mut stream: TcpStream;

    #[cfg(not(target_arch = "wasm32"))]
    {
        stream = TcpStream::connect_timeout(&socket_addr, Duration::from_secs(timeout))?;
        stream.set_write_timeout(Some(Duration::from_secs(timeout)))?;
        stream.set_read_timeout(Some(Duration::from_secs(timeout)))?;
    }
    #[cfg(target_arch = "wasm32")]
    {
        stream = TcpStream::connect_timeout(socket_addr, Duration::from_secs(timeout))?;
        stream.set_write_timeout(Some(Duration::from_secs(timeout)));
        stream.set_read_timeout(Some(Duration::from_secs(timeout)));
    }

    let request = format!("GET /plain/{} HTTP/1.1\r\n\r\n", sections.to_owned());
    stream.write(request.as_bytes())?;
    stream.flush()?;

    let resp: Response = serde_json::from_reader(stream)?;

    Ok(resp)
}

#[derive(Options, Debug)]
/// netdata plugin for olsrd monitoring
struct Args {
    /// seconds between measurements (required)
    #[options(free, required)]
    interval: u64,

    /// target address
    #[options(short = 'a', meta = "HOST:PORT", default = "localhost:9090")]
    addr: String,

    /// timeout for network requests
    #[options(short = 't', meta = "SECONDS", default = "1")]
    timeout: u64,

    /// print version and exit
    #[options(short = 'V')]
    version: bool,

    /// verbose logging
    #[options(short = 'v')]
    verbose: bool,

    /// suppresses output reporting
    #[options(short = 'q')]
    quiet: bool,

    /// debug
    #[options(short = 'd')]
    debug: bool,

    /// help
    #[options(help_flag, short = 'h')]
    help: bool,
}

/// Argument Parser and Config Reader
fn args_or_config() -> Args {
    // initialize logging
    Builder::new()
        .filter_level(LevelFilter::Trace)
        .format_timestamp(None)
        .init();

    let cmd: Vec<String> = std::env::args().collect();

    // wasi doesn't use argv[0] for command name
    #[cfg(not(target_arch = "wasm32"))]
    let args_r = Args::parse_args_default(&cmd[1..]);
    #[cfg(target_arch = "wasm32")]
    let args_r = Args::parse_args_default(&cmd);

    let args: Args = match args_r {
        Err(err) => {
            error!("{}", err);
            std::process::exit(0);
        }
        Ok(args) => args,
    };

    if args.debug {
        log::set_max_level(LevelFilter::Debug);
    } else if args.verbose {
        log::set_max_level(LevelFilter::Info);
    } else if args.quiet {
        log::set_max_level(LevelFilter::Off);
    } else {
        log::set_max_level(LevelFilter::Warn);
    }

    debug!("\n{:#?}", args);

    if args.help {
        eprintln!("{}", Args::usage());
        std::process::exit(0)
    }

    if args.version {
        eprintln!("Version: {}", env!("CARGO_PKG_VERSION"));
        std::process::exit(0)
    }

    args
}

/// replace dots in ip-addr by '_' to fullfil id-name constraints.
fn ip_to_id(ip: &str) -> String {
    ip.replace('.', "_")
}

fn main_loop(args: &Args, socket_addr: SocketAddr) -> Result<()> {
    let mut out = std::io::stdout();
    let mut collector = Collector::new(&mut out);

    loop {
        let resp: Response = simple_json_fetch(socket_addr, "neighbors/links", args.timeout)?;
        debug!("data found in response:\n{:#?}", &resp);

        for link in resp.links.unwrap() {
            match collector.prepare_value(
                "olsr.link_quality",
                &ip_to_id(&link.remote_IP),
                (link.link_quality * 1000.0).round() as i64,
            ) {
                Err(CollectorError::UnkownChart(_)) => {
                    // setup chart and dimension
                    collector.add_chart(&Chart {
                        type_id: "olsr.link_quality",
                        title: "link_quality (LQ)",
                        ..Default::default()
                    })
                }
                Err(CollectorError::UnkownDimension(_)) => {
                    // add only new dimension
                    collector.add_dimension(
                        "olsr.link_quality",
                        &Dimension {
                            id: &ip_to_id(&link.remote_IP),
                            name: &link.remote_IP,
                            divisor: Some(1000),
                            ..Default::default()
                        },
                    )
                }
                Err(e) => Err(e),
                Ok(()) => Ok(()),
            }?
        }
        collector.commit_chart("olsr.link_quality")?;

        thread::sleep(Duration::from_secs(args.interval));
    }
}

fn main() -> Result<()> {
    let args = args_or_config();

    // resolve hostname once at startup
    let mut addrs = match args.addr.to_socket_addrs() {
        Ok(addrs) => {
            debug!(
                "the first one of this IP addresses will be used for '{}':\n{:#?}",
                args.addr, addrs
            );
            addrs
        }
        Err(e) => {
            error!("Failed to resolve [host:port]: \"{}\"", args.addr);
            bail!(e);
        }
    };
    let socket_addr: SocketAddr = addrs.next().unwrap();

    // if no host address is explecitly given, 
    // we just test the default oslrd port on the local machine once
    // at startup and disable the plugin if we can't connect.
    if args.addr == Args::parse_args_default(&vec!["1"]).unwrap().addr {
        match simple_json_fetch(socket_addr, "links", args.timeout){
            Err(_) => {
                bail!("(ignored) No olsrd listening on default port -> disable plugin");
            }
            _ => {}
        };
    }

    main_loop(&args, socket_addr)?;

    Ok(())
}
